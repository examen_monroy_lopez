#!/usr/bin/env python

"""This script was used to produce the file
http://pzwart2.wdka.hro.nl/~ilopez/pypi.html
"""

if __name__ == "__main__":
    
    import lib
    
    number_of_tweets = 70
    screen_name = "pypi"
    config_file = "config.sample.ini"
    jinja_templates_folder = "."
    my_template = "template.html"
    my_html_file = screen_name + ".html"
    
    read_some_new_tweets = lib.ReadNewTweets(config_file, screen_name, number_of_tweets)
    read_some_new_tweets.read_config_file()
    list_of_tweets = read_some_new_tweets.read_user_timeline()
    
    write_new_tweets_to_db = lib.WriteNewTweetsToDB(config_file, list_of_tweets)
    write_new_tweets_to_db.read_config_file()
    write_new_tweets_to_db.write_tweets_to_db()
    
    read_tweets_from_db = lib.ReadTweetsFromDB(config_file)
    read_tweets_from_db.read_config_file()
    list_of_tweets = read_tweets_from_db.read_tweets_from_db()
    
    write_tweets_to_html_file = lib.WriteTweetsToHTMLFile(jinja_templates_folder,
                                                          my_template,
                                                          list_of_tweets,
                                                          my_html_file)
    write_tweets_to_html_file.autolink_list_of_tweets()
    write_tweets_to_html_file.write_html_file()
